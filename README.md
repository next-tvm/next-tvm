# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
*THE DESCRIPTION:
----------------

A simple websocket application making use of any popular Javascript framework that will receive 
publishing events from a backend server. Each of these data samples will look as follows:

{

  "id": 1001,
  "creator_id": "abcdef12345",
  "expiration_time": 4111111111,

}

The samples will be shown in a live grid, and each time a new one is received, it should be added 
to the top of the list, with a UX method that attracts the user’s attention to it. There is an 
expiration time in each packet, when the expiration time is met, the item should be removed 
gracefully from the grid.
* Version 1.0

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Thando Mahlati
* Website: http://www.next-tvm.co.za/
* Other community or team contact